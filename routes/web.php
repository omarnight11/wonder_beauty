<?php

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MainController;
use App\Http\Controllers\ReviewController;
use App\Http\Controllers\ProductController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Route::get('/', [MainController::class, 'index']);
Route::get('/products', [ProductController::class, 'index'])->name('product.index');
Route::get('/product/{id}', [ProductController::class, 'show'])->name('product.show');

Route::get('/reviews', [ReviewController::class, 'index'])->name('review.index');
Route::get('/add-review', [ReviewController::class, 'create'])->name('review.create');
Route::post('/reviews_store', [ReviewController::class, 'store'])->name('reviews.store');


Route::group(['namespace' => 'App\Http\Controllers'], function()
{   
    /**
     * Home Routes
     */
    Route::get('/admin', 'DashboardController@index')->name('dashboard.index');
    Route::post('/admin/update-threshold', 'DashboardController@updateThreshold')->name('dashboard.updateThreshold');
    Route::post('/admin/updateMode', 'DashboardController@updateMode')->name('dashboard.updateMode');
    Route::post('/admin/setMode', 'DashboardController@setMode')->name('dashboard.setMode');
    Route::delete('/decline-reviews/{id}', 'DashboardController@decline')->name('reviews.decline');
    Route::post('/approve-reviews/{id}', 'DashboardController@approve')->name('reviews.approve');




    Route::group(['middleware' => ['guest']], function() {
        /**
         * Register Routes
         */
        Route::get('/register', 'RegisterController@show')->name('register.show');
        Route::post('/register', 'RegisterController@register')->name('register.perform');

        /**
         * Login Routes
         */
        Route::get('/login', 'LoginController@show')->name('login.show');
        Route::post('/login', 'LoginController@login')->name('login.perform');

    });
    Route::group(['middleware' => ['auth']], function() {
        /**
         * Logout Routes
         */
        Route::get('/logout', 'LogoutController@perform')->name('logout.perform');
    });
});