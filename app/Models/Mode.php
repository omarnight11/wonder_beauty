<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mode extends Model
{
    protected $table = 'modes';
    protected $primaryKey = 'key';
    public $incrementing = false;
    public $timestamps = true;

    protected $fillable = ['key', 'value'];
}
