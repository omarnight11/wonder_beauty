<?php

namespace App\Http\Controllers;

use App\Models\Mode;
use App\Models\Setting;
use Illuminate\Http\Request;
use App\Models\PendingReview;


class DashboardController extends Controller
{
    public function index() 
        {
            $threshold = $this->getThreshold(); 
            $pendingReviews = PendingReview::all();
            $mode = Mode::where('key', 'mode')->value('value');
            
            return view('dashboard.index', compact('threshold','pendingReviews','mode'));
        }

    public function updateThreshold(Request $request)
        {
            $request->validate([
                'threshold' => 'required|integer|min:1', 
            ]);

            $threshold = $request->input('threshold');
            $this->setThreshold($threshold); 

            return redirect()->back()->with('success', 'Threshold updated successfully.');
        }

    private function getThreshold()
        {
            $setting = Setting::find('threshold');

            if ($setting) {
                return $setting->value;
            } else {
                return null;
            }
        }

    private function setThreshold($threshold)
        {
            $setting = Setting::updateOrCreate(
                ['key' => 'threshold'],
                ['value' => $threshold]
            );
            echo "Threshold set to: " . $threshold;
        }

    public function updateMode(Request $request)
        {
            $request->validate([
                'mode' => 'required|in:1,2',
            ]);
        
            $mode = $request->input('mode');
            $this->setMode($mode);
        
            return redirect()->back()->with('success', 'Mode updated successfully.');
        }
    
    private function setMode($mode)
        {
            $setting = Mode::updateOrCreate(
                ['key' => 'mode'],
                ['value' => $mode]
            );
            echo "Mode set to: " . $mode;
        }

    public function decline($id)
        {
            $review = PendingReview::findOrFail($id);
            $review->delete();
            
            return redirect()->back()->with('success', 'Review declined successfully.');
        }

        public function approve($id)
        {
            $review = PendingReview::findOrFail($id);
            
            $data = [
                'product_id' => $review->product_id,
                'comment' => $review->comment,
                'rating' => $review->rating
            ];
            
            $client = new \GuzzleHttp\Client();
            $response = $client->post('http://159.223.20.67:5000/api/reviews', [
                'headers' => [
                    'Content-Type' => 'application/json'
                ],
                'json' => $data
            ]);

            // dd($response->getStatusCode(), $response->getBody());

            
            if ($response->getStatusCode() === 201) {
                $review->delete();

                return redirect()->back()->with('success', 'Review Approved successfully.');
            } else {
                return redirect()->back()->with('error', 'Failed to approve the review.');
            }
        }
    

}
