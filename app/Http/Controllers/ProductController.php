<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class ProductController extends Controller
{
    public function index()
        {
            $response = Http::get('http://159.223.20.67:5000/api/products');

            if ($response->successful()) {
                $products = $response->json();

                $products = collect($products)->map(function ($product, $index) {
                    $product['id'] = $index + 1;
                    return $product;
                })->all();

            } else {
                abort(500, 'Failed to retrieve products from the API.');
            }

            return view('products', compact('products'));
        }


        public function show($id)
        {
            $productResponse = Http::get('http://159.223.20.67:5000/api/product/' . $id);

            if ($productResponse->successful()) {
                $product = $productResponse->json();
            } else {
                abort(500, 'Failed to retrieve the product details from the API.');
            }

            $reviewsResponse = Http::get('http://159.223.20.67:5000/api/reviews');

            if ($reviewsResponse->successful()) {
                $reviews = $reviewsResponse->json();
                $threshold = Setting::where('key', 'threshold')->value('value');

            } else {
                abort(500, 'Failed to retrieve the reviews from the API.');
            }

            return view('product-details', compact('product', 'reviews','threshold'));
        }


}
