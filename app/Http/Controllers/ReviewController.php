<?php

namespace App\Http\Controllers;

use App\Models\Mode;
use App\Models\Setting;
use Illuminate\Http\Request;
use App\Models\PendingReview;
use Illuminate\Support\Facades\Http;


class ReviewController extends Controller
{
    public function index()
        {
            $response = Http::get('http://159.223.20.67:5000/api/reviews');
            $reviews = $response->json();
            $threshold = Setting::where('key', 'threshold')->value('value');

            return view('reviews', compact('reviews','threshold'));
        }

    public function create()
        {
            $response = Http::get('http://159.223.20.67:5000/api/products');
            $products = $response->json();
            
            return view('add_review', compact('products'));
        }


        public function store(Request $request)
        {
            $request->validate([
                'product' => 'required',
                'rate' => 'required',
                'comment' => 'required',
            ]);

            $data = [
                'product_id' => (int)$request->input('product'), 
                'comment' => $request->input('comment'),
                'rating' => (int)$request->input('rate'), 
            ];

            $threshold = Setting::where('key', 'threshold')->value('value');
            $mode = Mode::where('key', 'mode')->value('value');

            if ($mode == 1) {
            
                if ($data['rating'] < $threshold) {

                    $pendingReview = new PendingReview();
                    $pendingReview->product_id = $data['product_id'];
                    $pendingReview->rating = $data['rating'];
                    $pendingReview->comment = $data['comment'];
                    $pendingReview->save();

                    return redirect()->back()->with('success', 'Review submitted successfully but requires approval.');
                }

                $response = Http::asJson()->post('http://159.223.20.67:5000/api/reviews', $data);
            }
            else{
                $pendingReview = new PendingReview();
                    $pendingReview->product_id = $data['product_id'];
                    $pendingReview->rating = $data['rating'];
                    $pendingReview->comment = $data['comment'];
                    $pendingReview->save();

                    return redirect()->back()->with('success', 'Review submitted successfully but requires approval.');
            }
            if ($response->successful()) {
                return redirect()->back()->with('success', 'Review submitted successfully.');
            } else {
                return redirect()->back()->with('error', 'Failed to submit the review. Please try again.');
            }
        }
}
