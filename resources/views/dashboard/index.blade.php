@extends('layouts.app-master')

@section('content')
<div class="container py-5">
    @auth
    <div class="card bg-light shadow-sm rounded p-4 mb-4">
        @if(session('success'))
        <div class="alert alert-success">{{ session('success') }}</div>
        @endif

        <h1 class="mb-4">Dashboard</h1>

        <div class="row">
            <div class="col-md-6">
                <h2>Threshold</h2>
                <form action="{{ route('dashboard.updateThreshold') }}" method="POST">
                    @csrf
                    <div class="mb-3">
                        <label for="threshold" class="form-label">Threshold:</label>
                        <input type="number" id="threshold" name="threshold" value="{{ $threshold }}" required min="1"
                            class="form-control">
                    </div>
                    <button type="submit" class="btn btn-primary">Update Threshold</button>
                </form>
            </div>

            <div class="col-md-6">
                <h2>Mode</h2>
                <form action="{{ route('dashboard.updateMode') }}" method="POST">
                    @csrf
                    <div class="mb-3">
                        <label for="mode" class="form-label">Mode:</label>
                        <select id="mode" name="mode" class="form-select">
                            <option value="1" {{ $mode == 1 ? 'selected' : '' }}>Automatic</option>
                            <option value="2" {{ $mode == 2 ? 'selected' : '' }}>Manual</option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary">Update Mode</button>
                </form>
            </div>
        </div>
    </div>

    <hr>

    <h2>Pending Reviews</h2>
    <table id="pendingReviews" class="table table-striped">
        <thead>
            <tr>
                <th>Product ID</th>
                <th>Rating</th>
                <th>Comment</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach($pendingReviews as $review)
            <tr>
                <td>{{ $review->product_id }}</td>
                <td>{{ $review->rating }}</td>
                <td>{{ $review->comment }}</td>
                <td>
                    <form action="{{ route('reviews.approve', $review->id) }}" method="POST" class="d-inline">
                        @csrf
                        @method('POST')
                        <button type="submit" class="btn btn-success btn-approve" data-review-id="{{ $review->id }}">approve</button>
                    </form>

                    <form action="{{ route('reviews.decline', $review->id) }}" method="POST" class="d-inline">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger btn-decline" data-review-id="{{ $review->id }}">Decline</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    @endauth

    @guest
    <div class="card bg-light shadow-sm rounded p-4">
        <h1>Homepage</h1>
        <p class="lead">You're viewing the home page. Please log in to view the restricted data.</p>
    </div>
    @endguest
</div>
@endsection


@section('scripts')
<script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap5.min.js"></script>
<script>
    $(document).ready(function () {
        $('#pendingReviews').DataTable();
    });
</script>

{{-- <script>
    document.addEventListener('DOMContentLoaded', function() {
        let approveButtons = document.getElementsByClassName('btn-approve');
        
        Array.from(approveButtons).forEach(function(button) {
            button.addEventListener('click', function() {
                let productId = this.getAttribute('data-product-id');
                let comment = this.getAttribute('data-comment');
                let rating = this.getAttribute('data-rating');
                
                let data = {
                    product_id: productId,
                    comment: comment,
                    rating: rating
                };
                
                let url = 'http://159.223.20.67:5000/api/reviews';
                
                fetch(url, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(data)
                })
                .then(function(response) {
                    if (response.ok) {
                        // Handle success response
                        console.log('Review approved successfully.');
                    } else {
                        // Handle error response
                        console.log('Failed to approve review.');
                    }
                })
                .catch(function(error) {
                    console.error('Error:', error);
                });
            });
        });
    }); 
    </script>--}}

@endsection


@section('styles')
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap5.min.css">
@endsection
