<!DOCTYPE html>
<html>
<head>
    <title>Product Details</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 20px;
        }

        .container {
            max-width: 800px;
            margin: 0 auto;
        }

        h1 {
            text-align: center;
            margin-bottom: 20px;
        }

        .product-details {
            display: flex;
            justify-content: center;
            align-items: center;
            flex-wrap: wrap;
        }

        .product-image {
            flex: 0 0 300px;
            margin-right: 20px;
        }

        .product-image img {
            width: 100%;
            height: auto;
        }

        .product-info {
            flex: 1;
        }

        .product-info h2 {
            margin-top: 0;
            margin-bottom: 10px;
        }

        .product-info p {
            margin-top: 0;
        }

        .reviews {
            margin-top: 40px;
        }

        .review {
            border: 1px solid #ddd;
            border-radius: 4px;
            padding: 10px;
            margin-bottom: 10px;
        }

        .review p {
            margin: 0;
        }

        .review .reviewer {
            font-weight: bold;
        }

        .star-rating {
        display: inline-block;
        font-size: 24px;
    }

    .star-rating::before {
        content: "\2605";
        color: #ddd;
    }

    .star-rating.filled::before {
        color: #FFD700;
    }

    .review-button {
            display: inline-block;
            float: right;
            padding: 10px 20px;
            font-size: 14px;
            font-weight: bold;
            text-decoration: none;
            color: #fff;
            background-color: #4caf50;
            border-radius: 4px;
            transition: background-color 0.3s ease;
            cursor: pointer;
        }
    </style>
</head>
<body>
    <div class="container">
        <h1>Product Details</h1>

        <div class="product-details">
            <div class="product-image">
                <img src="#" alt="Product Image">
            </div>

            <div class="product-info">
                <h2>{{ $product['product_name'] }}</h2>
                <p>Price: {{ $product['price'] }}</p>
                <p>{{ $product['description'] }}</p>
            </div>
        </div>

        <div class="reviews">
            <h2>Reviews</h2>
            @foreach ($reviews as $review)
            @if ($review['rating'] >= $threshold)
                @if ($review['product_id'] === $product['product_id'])
                    <div class="review">
                        <p>Rating:
                            <span>
                                @for ($i = 0; $i < $review['rating']; $i++)
                                    <span class="star-rating filled"></span>
                                @endfor
                            </span>
                        </p>
                        <p>Comment: {{ $review['comment'] }}</p>
                    </div>
                @endif
                @endif
            @endforeach
        </div>

        <button class="review-button" onclick="location.href='{{ route('review.create') }}'">Add Review</button>

    </div>
</body>
</html>
