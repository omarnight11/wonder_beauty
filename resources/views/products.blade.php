<!DOCTYPE html>
<html>
<head>
    <title>Products</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 20px;
        }

        h1 {
            text-align: center;
            margin-bottom: 20px;
        }

        .product-container {
            display: flex;
            flex-wrap: wrap;
            justify-content: center;
        }

        .product-square {
            width: 200px;
            height: 260px;
            border: 1px solid #ddd;
            border-radius: 4px;
            margin: 10px;
            padding: 10px;
            box-sizing: border-box;
            background-color: #f9f9f9;
            transition: box-shadow 0.3s;
            text-align: center;
        }

        .product-square:hover {
            box-shadow: 0 0 5px rgba(0, 0, 0, 0.3);
        }

        .product-square h2 {
            margin-top: 0;
            margin-bottom: 10px;
        }

        .product-square p {
            margin-top: 0;
        }

        .product-square .details-button {
            background-color: #4CAF50;
            border: none;
            color: white;
            padding: 8px 16px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 14px;
            margin-top: 10px;
            cursor: pointer;
            transition: background-color 0.3s;
        }

        .product-square .details-button:hover {
            background-color: #45a049;
        }
    </style>
</head>
<body>
    <h1>Products</h1>

    <div class="product-container">
        @foreach ($products as $product)
            <div class="product-square">
                <h2>{{ $product['product_name'] }}</h2>
                <p>Price: {{ $product['price'] }}</p>
                <button class="details-button" onclick="window.location.href='{{ route('product.show', ['id' => $product['id']]) }}'">Details</button>
            </div>
        @endforeach
    </div>
</body>
</html>
