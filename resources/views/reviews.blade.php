<!DOCTYPE html>
<html>
<head>
    <title>Reviews</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            padding: 20px;
        }

        .reviews-container {
            display: grid;
            grid-template-columns: repeat(3, 1fr);
            grid-gap: 20px;
        }

        .review {
            padding: 20px;
            border: 2px solid #000000;
            border-radius: 4px;
        }

        .details-button {
            display: inline-block;
            padding: 10px 20px;
            font-size: 14px;
            font-weight: bold;
            text-decoration: none;
            color: #fff;
            background-color: #4caf50;
            border-radius: 4px;
            transition: background-color 0.3s ease;
            cursor: pointer;
        }

        .details-button:hover {
            background-color: #45a049;
        }

        .rating-stars {
            display: inline-block;
            color: #ffc700;
        }
    </style>
</head>
<body>
    <h1>Reviews</h1>

    <div class="reviews-container">
        @foreach ($reviews as $review)
            @if ($review['rating'] >= $threshold)
                <div class="review">
                    <p style="font-weight: 700">Product: {{ $review['product_id'] }}</p>
                    <p>Comment: {{ $review['comment'] }}</p>
                    <p>Rating:
                        @for ($i = 0; $i < $review['rating']; $i++)
                            <span class="rating-stars">★</span>
                        @endfor
                    </p>
                    <a class="details-button" onclick="window.location.href='{{ route('product.show', ['id' => $review['product_id']]) }}'">Product Details</a>
                </div>
            @endif
        @endforeach
    </div>
</body>
</html>