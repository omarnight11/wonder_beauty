<!DOCTYPE html>
<html>
<head>
    <title>Check Products and Reviews</title>
    <style>
        body {
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            background-color: #f1f1f1;
            font-family: Arial, sans-serif;
        }

        .container {
            text-align: center;
        }

        .button {
            display: inline-block;
            padding: 20px 40px;
            font-size: 24px;
            font-weight: bold;
            text-decoration: none;
            color: #fff;
            background-color: #4caf50;
            border-radius: 8px;
            transition: background-color 0.3s ease;
        }

        .button:hover {
            background-color: #45a049;
        }
    </style>
</head>
<body>
    <div class="container">
        <a href="{{ route('product.index') }}" class="button">Check Products</a>
        <a href="{{ route('review.index') }}" class="button">Check Reviews</a>
        <a href="{{ route('login.show') }}" class="button">Admin login</a>
    </div>
</body>
</html>
